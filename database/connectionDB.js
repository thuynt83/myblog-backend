const sqlite3 = require("sqlite3").verbose();
// const { rejects } = require("assert");
// const { resolve } = require("path");
const path = require("path");

const dbPath = path.join(__dirname, "thuyblog.db");

const dbConnection = new sqlite3.Database(dbPath, (err) => {
    if (err) {
        console.log(err);
    } else {
        console.log("Connected to DB");
    }
});

module.exports = {
    dbConnection: dbConnection
}