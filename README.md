### DATABASE

Ctrl + shift + P : mở tùy chọn sử dụng DB

    1. system_parameter
        key {
            home_welcome_text
            home_hero_banner_img_url
            facebook_url
            instagram_url
            twitter_url
            linkedin_url
            description_login
            user_name
            user_author
            user_password
            user_img_url
            user_date_of_birth
            user_address
            user_job
            user_description
        }
        value

    2. tag
        tag_id
        tag_name

    3. post
        post_id
        tag_id
        tille
        time
        img_urls
        content

### TASK

## Backend

    - Khởi tạo một project bằng nodejs
    - Thư viện cần thiết nodemon
    - Làm sao để sử dụng các thư viện (global || local)?
    - Kết nối tới SQLITE
    - Sử dụng thư viện express để viết RestAPI
    - Tạo một API get
    - Các loại parameter
        . query parameter
        . path parameter
        . body parameter
    - GET
    - POST
    - PUT
    - DELETE
    - Làm sao để sử dụng các API trên (postman)
    - Làm sao để call API trên browser (để về sau code client)
    - Đi sâu vào phân tích một request và response

## Database

    - Tạo bảng
    - Sử dụng GUI
    - Thêm các row vào trong system_parameter
    - Thêm các dữ liệu giả lập vào tags và post
    - Sử dụng các query
        . Thêm dữ liệu
        . Sửa dữ liệu
        . Xóa dữ liệu
        . Truy vấn dữ liệu

## Backend + DB

    - asyn, await
    - Tạo kết nối tới Database
    - Thực thi các lệnh thêm, sửa, xóa, truy vấn

## GIT

    git add .  -> stage all changes
    git commit -m "<type commit here>"
    git push

## TOKEN

    $ node
    require('crypto').randomBytes(64).toString('hex')
