const express = require("express");
const bodyParser = require("body-parser");
const ApiRouter = require("./routers/api");
const app = express();


app.use(bodyParser.json());

app.use(ApiRouter);

const port = 8383;

app.listen(port, () => {
    console.log(`Running http://localhost:${port}`);

})