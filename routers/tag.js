const express = require("express");
const TagRouter = express.Router();
const { dbConnection } = require("../database/connectionDB");


// view list tags
TagRouter.get("/api/tags", (req, res) => {
    let selectQuery = `SELECT * FROM tag ORDER BY tag_id DESC`;
    dbConnection.all(selectQuery, (err, rows) => {
        // console.log("selectQuery", selectQuery);
        if (err) {
            return res.status(400).send({ error: err });
        } else {
            return res.status(200).send(rows);
        }
    });
});

// insert tag
TagRouter.post("/api/tags", (req, res) => {
    // read data from req.body
    const body = req.body;
    console.log("body", body)

    // insert into database and return message
    let insertQuery = ` INSERT INTO tag(tag_name) VALUES (?)`;
    dbConnection.run(insertQuery, [body.tag_name], (err) => {
        if (err) {
            return res.send("Failed to insert data");
        }
        return res.status(200).send("Successfully inserted data");






    });
});

// update tag by id
TagRouter.put("/api/tags/:id", (req, res) => {
    // read id from path
    const params = req.params;
    console.log("params", params);
    console.log("path", params.id);
    const idTag = params.id;
    // read updated data from req.body
    const body = req.body;
    // update data
    const updateQuery = `UPDATE tag SET tag_name = ? WHERE tag_id = ?`;
    dbConnection.run(updateQuery, [body.tag_name, idTag], (err) => {
        if (err) {
            return res.status(500).send(err);
        }
        return res.status(200).send("Updated data successfully");
    });
});

// delete tag by id
TagRouter.delete("/api/tags/:id", (req, res) => {
    // read id from path
    const params = req.params;
    console.log("params", params.id);

    // datele data
    let deleteQuery = `DELETE FROM tag WHERE tag_id = ?`;
    dbConnection.run(deleteQuery, [params.id], (err) => {
        if (err) {
            return res.status(500).send("Failed to delete data");
        }
        return res.status(200).send("Successfully deleted data");
    });
});

module.exports = TagRouter;