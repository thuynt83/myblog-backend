const express = require("express");
const ParameterRouter = express.Router();
const { dbConnection } = require("../database/connectionDB"); 


// view list system parameters
ParameterRouter.get("/api/systemparameters", (req, res) => {

    const selectQuery = `SELECT * FROM system_parameters`;
    dbConnection.all(selectQuery, (err, rows) => {
        if (err) {
            return res.status(400).send({ error: err });
        }
        return res.status(200).send(rows);
    });
});

// view system param by id
ParameterRouter.get("/api/systemparameters/:id", (req, res) => {
    // read id from path 
    const params = req.params;
    console.log("params", params.id);

    // get post 
    let selectQuery = `SELECT * FROM system_parameters WHERE key_id = ?`;
    dbConnection.get(selectQuery, [params.id], (err, rows) => {
        if (err) {
            return res.status(500).send("Failed to select data");
        }
        return res.status(200).send(rows);
    });
});

// update param by id
ParameterRouter.put("/api/systemparameters/:id", (req, res) => {
    // read id from path
    const params = req.params;
    console.log("params", params);
    console.log("path", params.id);
    const idParam = params.id;

    // read data from body
    const body = req.body;

    // update data in a colum: value
    let updateQuery = `UPDATE system_parameters SET value = ? WHERE key_id = ?`;
    dbConnection.run(updateQuery, [body.value, idParam], (err) => {
        if (err) {
            return res.status(500).send(err);
        }
        return res.status(200).send("Successfully updated data");
    });
});

module.exports = ParameterRouter;
