const express = require("express");
const PostRouter = express.Router();
const { dbConnection } = require("../database/connectionDB");

//view list posts
PostRouter.get("/api/posts", (req, res) => {
    const { title, tag_id } = req.query;
    const { pageNumber = 1, pageSize = 10 } = req.query;

    const titleCondition = `and title like '%${title}%'`;
    const conditionQuery = `
            from post
            where ${title ? titleCondition : ""}
            ORDER BY post_id DESC
            limit ${pageSize}
            offset ${(pageNumber - 1) * pageSize}
         `;

    const selectQuery = ` select * ` + conditionQuery;
    // const query = {};
    // if ()
    // let selectQuery = `SELECT * FROM post ORDER BY post_id DESC`;
    dbConnection.all(selectQuery, (err, rows) => {
        if (err) {
            return res.status(400).send({ error: err });
        }
        return res.status(200).send(rows);
    });


    // const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 10;
    // const pageNumber = req.query.pageNumber ? parseInt(req.query.pageNumber) : 1;

    // const title = req.query.title ? req.query.title : null;
    // const titleCondition = `and title like '%${title}%'`;

    // const conditionQuery = `
    //         from post
    //         where 1 = 1 
    //         ${title ? titleCondition : ""}
    //         limit ${pageSize}
    //         offset ${(pageNumber - 1) * pageSize}
    //     `;

    // const selectQuery = ` select * ` + conditionQuery;
    // const countQuery = ` select count(*) as total ` + conditionQuery;

    // console.log("countQuery", countQuery)
    // console.log("selectQuery", selectQuery)
    // // todo: viet them phan xu ly voi db
    // res.send("OK");
});

// view post by id
PostRouter.get("/api/posts/:id", (req, res) => {
    // read id from path 
    const params = req.params;
    console.log("params", params.id);
    // get post 
    let selectQuery = `SELECT * FROM post WHERE post_id = ?`;
    dbConnection.get(selectQuery, [params.id], (err, rows) => {
        if (err) {
            return res.status(500).send("Failed to select data");
        }
        return res.status(200).send(rows);
    });
});

// insert a post
PostRouter.post("/api/posts", (req, res) => {
    // read data from req.body
    const body = req.body;
    // insert a post 
    let insertQuery = `INSERT into post
                        (title, day_post, img_urls, content, tag_id)
                    VALUES
                        (?, ?, ?, ?, ?)`;  // Kiểm tra sự tồn tại của tag_id
    dbConnection.run(insertQuery, [body.title, body.day_post, body.img_urls, body.content, body.tag_id], (err) => {
        if (err) {
            return res.status(500).send("Failed to insert data");
        }
        return res.status(200).send("Successfully inserted data");
    });
});

// update post by id
PostRouter.put("/api/posts/:id", (req, res) => {
    // read id from path 
    const params = req.params;
    console.log("params", params)
    console.log("path", params.id);
    const idPost = params.id;
    // read data from body
    const body = req.body;
    // update data in a colum : tag_id
    let updateQuery = `UPDATE post SET tag_id = ? WHERE post_id = ?`; // cập nhật data theo trường dữ liệu
    dbConnection.run(updateQuery, [body.tag_id, idPost], (err) => {
        if (err) {
            return res.status(500).send(err);
        }
        return res.status(200).send("Successfully updated data");
    });
});

// delete post by id
PostRouter.delete("/api/posts/:id", (req, res) => {
    // read id from path 
    const params = req.params;
    console.log("path", params.id);
    // delete post 
    const deleteQuery = `DELETE FROM post WHERE post_id = ?`;
    dbConnection.run(deleteQuery, [params.id], (err) => {
        if (err) {
            return res.status(500).send(err);
        }
        return res.status(200).send("Deleted data successfully");
    });
});

module.exports = PostRouter;

