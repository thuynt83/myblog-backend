const express = require("express");
const ApiRouter = express.Router();

const TagRouter = require("./tag");
const PostRouter = require("./post");
const ParameterRouter = require("./parameter");

ApiRouter.get("/ping", (req, res) => {
    console.log("Handling methor get!");
    return res.send('pong');
});

ApiRouter.use(TagRouter);
ApiRouter.use(PostRouter);
ApiRouter.use(ParameterRouter);


module.exports = ApiRouter;